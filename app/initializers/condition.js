import ConditionAdapter from '../adapters/condition';

export default {
  name: 'condition',

  initialize: function(container, app) {
    app.register('adapter:condition', ConditionAdapter);
  }
};
