import Ember from 'ember';

var Router = Ember.Router.extend({
  location: WeatherENV.locationType
});

Router.map(function() {
  this.resource('condition', { path: 'conditions/:condition_id' });
});

export default Router;
